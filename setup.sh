#!/bin/bash

echo $PWD;

this_dir=$PWD;
html_dir=/var/www/html/

cd $html_dir;

echo $PWD;
echo $this_dir;

# initialize the web directory
if [ -d ./web ] ; then
	rm -fr ./web
        rm -fr ./ide
else
	mkdir ./web
fi

# get the IP of the install
ip=$(wget http://ipinfo.io/ip -qO -) 

# do the wordpress install

wp core download --path=$html_dir/web --allow-root;
wp core config --path=$html_dir/web --dbname=wpdatabase --dbuser=root --dbpass=root --locale=en_US --allow-root
wp core install --path=$html_dir/web --url=http://$ip/web --title="TopTal Wordpress Tech1" --admin_password=wordpress --admin_email=tech1@toptal.com --admin_user=admin --allow-root

#Lets do the ide

cd $html_dir;

git clone https://github.com/Codiad/Codiad  ide

# copy over preconfigured files
cp $this_dir/ide_files/* $html_dir/ide/ -r
echo "--IDE configuration files copied--"

# make sure is  owned by  www-data
chown www-data:www-data ./ -R

